"use strict";
/*!

Creata - 0.1.0 - 03 AUG 2015

Components files creation tool

-------------------------------------------------- */

var gulp        = require('gulp'),
    uglify      = require('gulp-uglify'),
    gulpif      = require('gulp-if'),
    argv        = require('yargs').argv,
    rename      = require('gulp-rename'),
    notify      = require("gulp-notify"),
    sass        = require('gulp-sass'),
    plumber     = require('gulp-plumber'),
    file = require('gulp-file'),
    del         = require('del'),
    yessets     = require('./../yessets.json');
    
 
var projectInfo = yessets.projectInfo ? yessets.projectInfo : {name:'NA', version:'NA'},
    notifyTitle = "Yesset for "+projectInfo.name,
    prependText = '/*!\n '+ projectInfo.name +'\n version: ' + projectInfo.version +'\n --------------------------------------------------------------------- \n Build Timestamp: ' + Math.floor(new Date().getTime()/1000) + '\n Build Date: ' + new Date().toDateString() +' ' + new Date().toTimeString() + '\n--------------------------------------------------------------------- */\n';

gulp.task('test', function() {
  var str = "hello vinod";
 
  return file('primus'+Math.random()+'.js', str, { src: true }).pipe(gulp.dest('dist/components'));
});

// What tasks does running gulp trigger?
gulp.task('default', taskCollection);